openapi: 3.0.1
info:
  description: Specification of the CRUD interface
  title: Users CRUD interface
  version: 1.0.0
servers:
- url: /api
tags:
- description: Users management operations (CRUD).
  name: users
paths:
  /users:
    get:
      description: Gets all users data
      operationId: getAllUsers
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserListResponse'
          description: Success
        "400":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: Bad request
        "401":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: Unauthorized
        "422":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: "Unprocessable entity. Codes: USER_ALREADY_EXISTS"
      security:
      - basicAuth: []
      summary: Get users list
      tags:
      - users
      x-accepts: application/json
      x-tags:
      - tag: users
    post:
      description: Create new user
      operationId: createUser
      requestBody:
        content:
          '*/*':
            schema:
              $ref: '#/components/schemas/CreateRequest'
        description: User object that has to be added
        required: true
      responses:
        "201":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserResponse'
          description: User created successfully
        "400":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: Bad request
        "401":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: Unauthorized
        "422":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: "Unprocessable entity. Codes: USER_ALREADY_EXISTS"
      security:
      - basicAuth: []
      summary: Create
      tags:
      - users
      x-codegen-request-body-name: body
      x-content-type: '*/*'
      x-accepts: application/json
      x-tags:
      - tag: users
  /users/{id}:
    delete:
      description: Removes user
      operationId: deleteUser
      parameters:
      - in: path
        name: id
        required: true
        schema:
          format: uuid
          type: string
      responses:
        "204":
          content: {}
          description: No content
        "400":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: Bad request
        "401":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: Unauthorized
        "404":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: User not found
        "422":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: Unprocessable entity.
      security:
      - bearerAuth: []
      summary: Delete user
      tags:
      - users
      x-accepts: application/json
      x-tags:
      - tag: users
    get:
      description: Gets specified user data
      operationId: getUserById
      parameters:
      - in: path
        name: id
        required: true
        schema:
          format: uuid
          type: string
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserResponse'
          description: Success
        "400":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: Bad request
        "401":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: Unauthorized
        "404":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: User not found
        "422":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: "Unprocessable entity. Codes: USER_ALREADY_EXISTS"
      security:
      - basicAuth: []
      summary: Get user
      tags:
      - users
      x-accepts: application/json
      x-tags:
      - tag: users
    put:
      description: Update user data
      operationId: updateUser
      parameters:
      - in: path
        name: id
        required: true
        schema:
          format: uuid
          type: string
      requestBody:
        content:
          '*/*':
            schema:
              $ref: '#/components/schemas/UpdateRequest'
        required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserResponse'
          description: Success
        "400":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: Bad request
        "401":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: Unauthorized
        "404":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: User not found
        "422":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: Unprocessable entity.
      security:
      - bearerAuth: []
      summary: Update user
      tags:
      - users
      x-codegen-request-body-name: body
      x-content-type: '*/*'
      x-accepts: application/json
      x-tags:
      - tag: users
components:
  schemas:
    User:
      example:
        personalId: "92011165987"
        surname: Musk
        citizenship: PL
        name: Elon
        id: 046b6c7f-0b8a-43b9-b35d-6489e6daee91
        age: 10
        email: e_musk@gmail.com
      properties:
        id:
          format: uuid
          type: string
        name:
          example: Elon
          type: string
        surname:
          example: Musk
          type: string
        age:
          example: 10
          minimum: 1
          type: integer
        personalId:
          example: "92011165987"
          pattern: "^[0-9]{11}$"
          type: string
        citizenship:
          enum:
          - PL
          - DE
          - UK
          example: PL
          pattern: "^[A-Z]{2}$"
          type: string
        email:
          example: e_musk@gmail.com
          pattern: "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$"
          type: string
      required:
      - age
      - citizenship
      - name
      - personalId
      - surname
      type: object
      xml:
        name: User
    Error:
      properties:
        responseHeader:
          $ref: '#/components/schemas/ResponseHeader'
        code:
          example: NOT_FOUND
          type: string
        message:
          example: Resource doesn't exist
          type: string
      required:
      - code
      - responseHeader
      type: object
    RequestHeader:
      example:
        sendDate: 2000-01-23T04:56:07.000+00:00
        requestId: 046b6c7f-0b8a-43b9-b35d-6489e6daee91
      properties:
        requestId:
          format: uuid
          type: string
        sendDate:
          description: "Date format according to ISO_8601 for example: yyyy-MM-dd'T'HH:mm:ss.SSSZ"
          format: date-time
          type: string
      required:
      - requestId
      - sendDate
      type: object
    ResponseHeader:
      properties:
        requestId:
          format: uuid
          type: string
        sendDate:
          description: "Date format according to ISO_8601 for example: yyyy-MM-dd'T'HH:mm:ss.SSSZ"
          format: date-time
          type: string
      required:
      - requestId
      - sendDate
      type: object
    CreateRequest:
      example:
        requestHeader:
          sendDate: 2000-01-23T04:56:07.000+00:00
          requestId: 046b6c7f-0b8a-43b9-b35d-6489e6daee91
        user:
          personalId: "92011165987"
          surname: Musk
          citizenship: PL
          name: Elon
          id: 046b6c7f-0b8a-43b9-b35d-6489e6daee91
          age: 10
          email: e_musk@gmail.com
      properties:
        requestHeader:
          $ref: '#/components/schemas/RequestHeader'
        user:
          $ref: '#/components/schemas/User'
      required:
      - requestHeader
      - user
      type: object
    UpdateRequest:
      example:
        requestHeader:
          sendDate: 2000-01-23T04:56:07.000+00:00
          requestId: 046b6c7f-0b8a-43b9-b35d-6489e6daee91
        user:
          personalId: "92011165987"
          surname: Musk
          citizenship: PL
          name: Elon
          id: 046b6c7f-0b8a-43b9-b35d-6489e6daee91
          age: 10
          email: e_musk@gmail.com
      properties:
        requestHeader:
          $ref: '#/components/schemas/RequestHeader'
        user:
          $ref: '#/components/schemas/User'
      required:
      - requestHeader
      - user
      type: object
    UserResponse:
      example:
        responseHeader:
          sendDate: 2000-01-23T04:56:07.000+00:00
          requestId: 046b6c7f-0b8a-43b9-b35d-6489e6daee91
        user:
          personalId: "92011165987"
          surname: Musk
          citizenship: PL
          name: Elon
          id: 046b6c7f-0b8a-43b9-b35d-6489e6daee91
          age: 10
          email: e_musk@gmail.com
      properties:
        responseHeader:
          $ref: '#/components/schemas/RequestHeader'
        user:
          $ref: '#/components/schemas/User'
      required:
      - responseHeader
      - user
      type: object
    UserListResponse:
      example:
        usersList:
        - personalId: "92011165987"
          surname: Musk
          citizenship: PL
          name: Elon
          id: 046b6c7f-0b8a-43b9-b35d-6489e6daee91
          age: 10
          email: e_musk@gmail.com
        - personalId: "92011165987"
          surname: Musk
          citizenship: PL
          name: Elon
          id: 046b6c7f-0b8a-43b9-b35d-6489e6daee91
          age: 10
          email: e_musk@gmail.com
        responseHeader:
          sendDate: 2000-01-23T04:56:07.000+00:00
          requestId: 046b6c7f-0b8a-43b9-b35d-6489e6daee91
      properties:
        responseHeader:
          $ref: '#/components/schemas/RequestHeader'
        usersList:
          items:
            $ref: '#/components/schemas/User'
          type: array
      required:
      - responseHeader
      - usersList
      type: object
  securitySchemes:
    basicAuth:
      scheme: basic
      type: http
    bearerAuth:
      in: header
      name: Bearer
      type: apiKey
x-original-swagger-version: "2.0"
