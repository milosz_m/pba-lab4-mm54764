package org.openapitools.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EncryptionResponse {

    private EncryptionData encryptionData;

    public EncryptionResponse() {
        super();
    }

    /**
     * Constructor with only required parameters
     */
    public EncryptionResponse(EncryptionData encryptionData) {
        this.encryptionData = encryptionData;
    }

    public EncryptionResponse encryptionData(EncryptionData encryptionData) {
        this.encryptionData = encryptionData;
        return this;
    }

    /**
     * Get encryptionData
     *
     * @return encryptionData
     */
    @NotNull @Valid
    @Schema(name = "encryptionData", requiredMode = Schema.RequiredMode.REQUIRED)
    @JsonProperty("encryptionData")
    public EncryptionData getEncryptionData() {
        return encryptionData;
    }

    public void setEncryptionData(EncryptionData encryptionData) {
        this.encryptionData = encryptionData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EncryptionResponse encryptionResponse = (EncryptionResponse) o;
        return Objects.equals(this.encryptionData, encryptionResponse.encryptionData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(encryptionData);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class EncryptionResponse {\n");
        sb.append("    encryptionData: ").append(toIndentedString(encryptionData)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
