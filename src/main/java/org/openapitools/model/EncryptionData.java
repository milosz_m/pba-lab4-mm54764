package org.openapitools.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class EncryptionData {

    private String encryptedSessionKey;
    private String encryptedData;

    public EncryptionData() {
        super();
    }

    /**
     * Constructor with only required parameters
     */
    public EncryptionData(String encryptedSessionKey, String encryptedData) {
        this.encryptedSessionKey = encryptedSessionKey;
        this.encryptedData = encryptedData;
    }

    public EncryptionData encryptedSessionKey(String encryptedSessionKey) {
        this.encryptedSessionKey = encryptedSessionKey;
        return this;
    }

    /**
     * Get encryptedSessionKey
     * @return encryptedSessionKey
     */
    @NotNull
    @Schema(name = "encryptedSessionKey", requiredMode = Schema.RequiredMode.REQUIRED)
    @JsonProperty("encryptedSessionKey")
    public String getEncryptedSessionKey() {
        return encryptedSessionKey;
    }

    public void setEncryptedSessionKey(String encryptedSessionKey) {
        this.encryptedSessionKey = encryptedSessionKey;
    }

    public EncryptionData encryptedData(String encryptedData) {
        this.encryptedData = encryptedData;
        return this;
    }

    /**
     * Get encryptedData
     * @return encryptedData
     */
    @NotNull
    @Schema(name = "encryptedData", requiredMode = Schema.RequiredMode.REQUIRED)
    @JsonProperty("encryptedData")
    public String getEncryptedData() {
        return encryptedData;
    }

    public void setEncryptedData(String encryptedData) {
        this.encryptedData = encryptedData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EncryptionData encryptionData = (EncryptionData) o;
        return Objects.equals(this.encryptedSessionKey, encryptionData.encryptedSessionKey) &&
                Objects.equals(this.encryptedData, encryptionData.encryptedData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(encryptedSessionKey, encryptedData);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class EncryptionData {\n");
        sb.append("    encryptedSessionKey: ").append(toIndentedString(encryptedSessionKey)).append("\n");
        sb.append("    encryptedData: ").append(toIndentedString(encryptedData)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
