package org.openapitools.api;

import org.openapitools.exceptions.HmacSignatureFailed;
import org.openapitools.exceptions.NotFound;
import org.openapitools.exceptions.UserAlreadyExists;
import org.openapitools.model.Error;
import org.openapitools.model.ResponseHeader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.OffsetDateTime;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionMapper {


    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Error> handleException() {
        Error error = new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("SERVER_ERROR")
                .message("Something went wrong");
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(UserAlreadyExists.class)
    public ResponseEntity<Error> handleBusinessException(UserAlreadyExists ex) {
        Error error = new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("USER_ALREADY_EXISTS")
                .message(ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(NotFound.class)
    public ResponseEntity<Error> handleNotFoundException() {
        Error error = new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("NOT_FOUND")
                .message("Not Found");
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(HmacSignatureFailed.class)
    public ResponseEntity<Error> handleHmacSignatureFailure(HmacSignatureFailed ex) {
        Error error = new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("UNAUTHORIZED")
                .message(ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Error> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader()
                .requestId(UUID.randomUUID())
                .sendDate(OffsetDateTime.now()));

        Map<String, Set<String>> fieldErrors = ex.getBindingResult().getFieldErrors()
                .stream()
                .collect(Collectors.groupingBy(FieldError::getField,
                        Collectors.mapping(FieldError::getDefaultMessage, Collectors.toSet())));

        String errorMessage = fieldErrors.entrySet()
                .stream()
                .map(entry -> entry.getKey() + ": " + String.join("; ", entry.getValue()))
                .collect(Collectors.joining("; "));

        error.setCode("VALIDATION_FAILED");
        error.setMessage(errorMessage);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }


}

