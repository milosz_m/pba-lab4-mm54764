package org.openapitools.api;

import org.openapitools.model.EncryptionData;
import org.openapitools.model.EncryptionResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

@RestController
public class CryptoController {
    private KeyPair keyPair;

    @PostMapping(value = "/encrypt", produces = MediaType.APPLICATION_JSON_VALUE)
    public EncryptionResponse encrypt(@RequestBody String request) {
        try {
            if (keyPair == null) {
                keyPair = generateKeyPair();
            }
            SecretKey sessionKey = generateSessionKey();
            System.out.println("KeyPair Encrypt:" + keyPair);
            Cipher rsaCipher = Cipher.getInstance("RSA");
            rsaCipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());
            byte[] encryptedSessionKey = rsaCipher.doFinal(sessionKey.getEncoded());

            Cipher aesCipher = Cipher.getInstance("AES");
            aesCipher.init(Cipher.ENCRYPT_MODE, sessionKey);
            byte[] encryptedData = aesCipher.doFinal(request.getBytes());

            String base64EncryptedSessionKey = Base64.getEncoder().encodeToString(encryptedSessionKey);
            String base64EncryptedData = Base64.getEncoder().encodeToString(encryptedData);

            // Creating an instance of EncryptionData
            EncryptionData encryptionData = new EncryptionData(base64EncryptedSessionKey, base64EncryptedData);

            // Returning the response with EncryptionData
            return new EncryptionResponse(encryptionData);
        } catch (Exception e) {
            e.printStackTrace();
            // In case of an error, return an instance of EncryptionResponse with an error message
            throw new RuntimeException("Error while encrypting data");
        }
    }

    @PostMapping("/decrypt")
    public String decrypt(@RequestBody EncryptionData encryptedData) {
        try {
            System.out.println("Received encryptedData: " + encryptedData);
            System.out.println("KeyPair Decrypt:" + keyPair);

            // Decode Base64-encoded strings
            byte[] decodedSessionKey = Base64.getDecoder().decode(encryptedData.getEncryptedSessionKey());
            byte[] decodedEncryptedData = Base64.getDecoder().decode(encryptedData.getEncryptedData());

            // Log the decoded keys
            System.out.println("Decoded Session Key: " + Arrays.toString(decodedSessionKey));
            System.out.println("Decoded Encrypted Data: " + Arrays.toString(decodedEncryptedData));

            // Initialize Cipher for RSA decryption using the private key
            Cipher rsaCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            rsaCipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());
            byte[] decryptedSessionKey = rsaCipher.doFinal(decodedSessionKey);

            // Log the decrypted session key
            System.out.println("Decrypted Session Key: " + Arrays.toString(decryptedSessionKey));

            // Initialize Cipher for AES decryption using the decrypted session key
            Cipher aesCipher = Cipher.getInstance("AES");
            aesCipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(decryptedSessionKey, "AES"));
            byte[] decryptedData = aesCipher.doFinal(decodedEncryptedData);

            // Log the decrypted data
            System.out.println("Decrypted Data: " + Arrays.toString(decryptedData));
            String base64EncryptedSessionKey = Base64.getEncoder().encodeToString(decryptedSessionKey);
            String base64EncryptedData = Base64.getEncoder().encodeToString(decryptedData);
            // Creating an instance of EncryptionData with decrypted data
            EncryptionData decryptedEncryptionData = new EncryptionData(new String(decryptedSessionKey), new String(decryptedData));

            // Returning the response with decrypted EncryptionData
            return new String(decryptedData);
        } catch (BadPaddingException e) {
            // Log specific details for BadPaddingException
            e.printStackTrace();
            System.err.println("BadPaddingException: Given final block not properly padded");
            throw new RuntimeException("Error while decrypting data: BadPaddingException");
        } catch (Exception e) {
            // Log other exceptions
            e.printStackTrace();
            throw new RuntimeException("Error while decrypting data");
        }
    }


    private KeyPair generateKeyPair() {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            return keyPairGenerator.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Error generating KeyPair", e);
        }
    }

    private SecretKey generateSessionKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(256);
        return keyGenerator.generateKey();
    }
}
