package org.openapitools.api;

import org.openapitools.exceptions.NotFound;
import org.openapitools.exceptions.UserAlreadyExists;
import org.openapitools.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import javax.annotation.Generated;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-12-02T15:51:35.540841+01:00[Europe/Warsaw]")
@Controller
@RequestMapping("${openapi.usersCRUDInterface.base-path:/api}")
public class UsersApiController implements UsersApi {

    private final NativeWebRequest request;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    public UsersApiController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    public ResponseEntity<UserResponse> createUser(
            CreateRequest body
    ) {

        User userFromBody = body.getUser();
        RequestHeader requestHeaderFromBody = body.getRequestHeader();
        User newUser = new User();
        UUID id = userFromBody.getId();
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            throw new UserAlreadyExists("User with id " + id + " already exists");
        }
        newUser.setId(id);
        newUser.setName(userFromBody.getName());
        newUser.setSurname(userFromBody.getSurname());
        newUser.setAge(userFromBody.getAge());
        newUser.setPersonalId(userFromBody.getPersonalId());
        newUser.setCitizenship(userFromBody.getCitizenship());

        newUser.setEmail(userFromBody.getEmail());

        UserResponse response = new UserResponse();

        response.setUser(newUser);
        response.setResponseHeader(requestHeaderFromBody);

        User savedUser = userRepository.save(newUser);

        return ResponseEntity.status(HttpStatus.CREATED).body(response);

    }
    public ResponseEntity<Void> deleteUser(UUID id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (!userOptional.isPresent()) {
            throw new NotFound();
        }

        try {
            userRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (DataIntegrityViolationException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    public ResponseEntity<UserListResponse> getAllUsers() {

        UserListResponse response = new UserListResponse();
        List<User> users = userRepository.findAll();

        String isoDateString = OffsetDateTime.now().toString();

        response.setUsersList(users);
        response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID()).sendDate(OffsetDateTime.now()));
        return ResponseEntity
                .ok(response);

    }

    public ResponseEntity<UserResponse> getUserById(UUID id) {
        Optional<User> userOptional = userRepository.findById(id);

        if (!userOptional.isPresent()) {
            throw new NotFound();
        }
            User user = userOptional.get();
            UserResponse response = new UserResponse();
            response.setUser(user);
            response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID()).sendDate(OffsetDateTime.now()));
            return ResponseEntity.ok(response);
    }
    public ResponseEntity<UserResponse> updateUser(UUID id, UpdateRequest body) {
        Optional<User> userOptional = userRepository.findById(id);

        if (!userOptional.isPresent()) {
            throw new NotFound();
        }
            User existingUser = userOptional.get();
            User userFromBody = body.getUser();
            if (userFromBody.getName() != null) {
                existingUser.setName(userFromBody.getName());
            }
            if (userFromBody.getSurname() != null) {
                existingUser.setSurname(userFromBody.getSurname());
            }
            if (userFromBody.getCitizenship() != null) {
                existingUser.setCitizenship(userFromBody.getCitizenship());
            }
            if (userFromBody.getAge() != null) {
                existingUser.setAge(userFromBody.getAge());
            }
            if (userFromBody.getPersonalId() != null) {
                existingUser.setPersonalId(userFromBody.getPersonalId());
            }

            User updatedUser = userRepository.save(existingUser);

            UserResponse response = new UserResponse();
            response.setUser(updatedUser);
            response.setResponseHeader(new RequestHeader().requestId(UUID.randomUUID()).sendDate(OffsetDateTime.now()));

            return ResponseEntity.ok(response);

    }

    }

