package org.openapitools.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.UUID;

@Component
public class CustomAuthenticationEntryPoint implements org.springframework.security.web.AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
        // Set the response status and content type
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType("application/json");

        // Generate requestId and sendDate
        String requestId = UUID.randomUUID().toString();
        String sendDate = OffsetDateTime.now().toString();

        // Construct response JSON
        String jsonResponse = String.format(
                "{" +
                        "\"responseHeader\": {\"requestId\": \"%s\", \"sendDate\": \"%s\"}, " +
                        "\"error\": \"Unauthorized\"" +
                        "}", requestId, sendDate);

        // Write the custom JSON response
        response.getWriter().write(jsonResponse);
    }
}
