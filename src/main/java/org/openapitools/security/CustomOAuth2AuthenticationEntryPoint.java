package org.openapitools.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomOAuth2AuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
        // Set the response status and content type
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); // or any other status
        response.setContentType("application/json");

        // Custom response body
        response.getWriter().write("{\"error\": \"Custom OAuth2 error message\"}");
    }
}
