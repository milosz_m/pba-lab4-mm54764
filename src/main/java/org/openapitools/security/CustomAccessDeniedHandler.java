package org.openapitools.security;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.UUID;

public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException {
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        response.setContentType("application/json");

        // Generate requestId and sendDate
        String requestId = UUID.randomUUID().toString();
        String sendDate = OffsetDateTime.now().toString();

        // Construct response JSON
        String jsonResponse = String.format(
                "{" +
                        "\"responseHeader\": {\"requestId\": \"%s\", \"sendDate\": \"%s\"}, " +
                        "\"error\": \"Forbidden\"" +
                        "}", requestId, sendDate);

        // Write the custom JSON response
        response.getWriter().write(jsonResponse);
    }
}