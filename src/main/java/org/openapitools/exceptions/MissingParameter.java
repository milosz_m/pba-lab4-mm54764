package org.openapitools.exceptions;

public class MissingParameter extends RuntimeException {
    public MissingParameter(String message) {
        super(message);
    }
}
