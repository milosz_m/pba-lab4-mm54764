package org.openapitools.exceptions;

public class HmacSignatureFailed extends RuntimeException {
    public HmacSignatureFailed(String message) {
        super(message);
    }
}
