package org.openapitools.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.crypto.Cipher;
import java.security.KeyPair;
import java.security.KeyPairGenerator;

@Configuration
public class CryptoConfig {

    @Bean
    public KeyPair keyPair() throws Exception {
        // Generowanie kluczy RSA
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        return keyPairGenerator.generateKeyPair();
    }

    @Bean
    public Cipher rsaCipher() throws Exception {
        // Utworzenie instancji Cipher dla RSA
        return Cipher.getInstance("RSA/ECB/PKCS1Padding");
    }

    @Bean
    public Cipher aesCipher() throws Exception {
        // Utworzenie instancji Cipher dla AES
        return Cipher.getInstance("AES");
    }
}
