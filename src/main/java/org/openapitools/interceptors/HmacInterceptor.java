package org.openapitools.interceptors;

import org.apache.commons.codec.binary.Hex;
import org.openapitools.exceptions.HmacSignatureFailed;
import org.openapitools.filters.HmacBodyWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class HmacInterceptor implements HandlerInterceptor   {
    private static Logger log = LoggerFactory.getLogger(HmacInterceptor.class);
    private static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";
    private static final String SECRET_KEY = "123456";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            // Skip HMAC verification for non-POST requests
            return true;
        }
        if (!(request instanceof HmacBodyWrapper wrapper)) {
            log.warn("Request is not an instance of HmacBodyWrapper. Request body cannot be read.");
            return true;
        }

        String body = wrapper.getRequestBody();
        String receivedHmacSignature = request.getHeader("X-HMAC-SIGNATURE");

        if (receivedHmacSignature == null || receivedHmacSignature.isEmpty()) {
            log.warn("No HMAC signature provided in the request.");
            throw new HmacSignatureFailed("No HMAC signature provided");
        }

        try {
            String generatedHmacSignature = generateHmacSignature(body, SECRET_KEY);
            if (!generatedHmacSignature.equals(receivedHmacSignature)) {
                log.warn("HMAC signature verification failed.");
                throw new HmacSignatureFailed("Invalid HMAC signature");
            }
            log.info("HMAC signature verification successful.");
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            log.error("Error generating HMAC signature: ", e);
            throw new RuntimeException();
        }

        return true;
    }

    private String generateHmacSignature(String data, String key) throws NoSuchAlgorithmException, InvalidKeyException, InvalidKeyException {
        Mac sha256Hmac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
        SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), HMAC_SHA256_ALGORITHM);
        sha256Hmac.init(secretKey);
        byte[] signedBytes = sha256Hmac.doFinal(data.getBytes());
        return Hex.encodeHexString(signedBytes);
    }
}