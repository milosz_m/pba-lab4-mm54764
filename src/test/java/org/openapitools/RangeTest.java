package org.openapitools;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RangeTest {

    public static class Range {

        private final int min;
        private final int max;

        public Range(int min, int max) {
            if (min > max) {
                throw new IllegalArgumentException("Invalid range: min cannot be greater than max");
            }
            this.min = min;
            this.max = max;
        }

        public boolean isInRange(int number) {
            return number >= min && number <= max;
        }
    }


    private Range range;

    @Before
    public void setUp() {
        range = new Range(5, 10);
    }

    @Test
    public void testIsInRangeForNumberInsideRange() {
        assertTrue(range.isInRange(7));
    }

    @Test
    public void testIsInRangeForNumberOutsideRange() {
        assertFalse(range.isInRange(15));
    }

    @Test
    public void testIsInRangeForNumberAtLowerBound() {
        assertTrue(range.isInRange(5));
    }

    @Test
    public void testIsInRangeForNumberAtUpperBound() {
        assertTrue(range.isInRange(10));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorThrowsExceptionForInvalidRange() {
        new Range(15, 10);
    }
}
