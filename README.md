# Sprawozdanie lab4-5

## Lab4

### Zadanie 3
Przygotować sprawozdanie, które zawierać będzie polecenia ```curl ``` umożliwiające wywołanie zaimplementowanych interfejsów. Do każdej z metod należy umieścić dwa polecenia:
1. Powodujące określony błąd wywołania aplikacji np. (400 „Bad Request” spowodowany zapytaniem, które nie posiada wymaganych pól).



2. Zwracające poprawny wynik zapytania

- Przkładowe ID użytkownika do testów: ```6d6f5582-fb42-4ee9-a1f4-d870cc7b9836```

**CREATE USER**
---
<span style="color: green;">SUCCESS</span>

```bash
#!/bin/bash

# Response vars
guid=$(uuidgen)
sendDate=$(date -u +"%Y-%m-%dT%H:%M:%SZ")

# Auth vars
username="mm54764"
password="123456"

apiEndpoint="http://localhost:8080/api/users"
jsonPayload=$(cat <<EOF
{
    "requestHeader": {
        "requestId": "$guid",
        "sendDate": "$sendDate"
    },
    "user": {
        "id": "6d6f5582-fb42-4ee9-a1f4-d870cc7b9837",
        "personalId": "92011165987",
        "surname": "Musk",
        "citizenship": "PL",
        "name": "Elon",
        "age": 10,
        "email": "e_musk@gmail.com"
    }
}
EOF
)

response=$(curl -X POST -u "$username:$password" "$apiEndpoint" -H "Content-Type: application/json" -d "$jsonPayload")

if [ $? -eq 0 ]; then
    echo "Request successful."
    echo "Response: $response"
else
    echo "Request failed."
fi

```
<span style="color: red;">ERROR - no surname</span>

```bash
#!/bin/bash

# Response vars
guid=$(uuidgen)
sendDate=$(date -u +"%Y-%m-%dT%H:%M:%SZ")

# Auth vars
username="mm54764"
password="123456"

apiEndpoint="http://localhost:8080/api/users"
jsonPayload=$(cat <<EOF
{
    "requestHeader": {
        "requestId": "$guid",
        "sendDate": "$sendDate"
    },
    "user": {
        "id": "6d6f5582-fb42-4ee9-a1f4-d870cc7b9837",
        "personalId": "92011165987",
        "citizenship": "PL",
        "name": "Elon",
        "age": 10,
        "email": "e_musk@gmail.com"
    }
}
EOF
)

response=$(curl -X POST -u "$username:$password" "$apiEndpoint" -H "Content-Type: application/json" -d "$jsonPayload")

if [ $? -eq 0 ]; then
    echo "Request successful."
    echo "Response: $response"
else
    echo "Request failed."
fi

```

**GET ALL USERS**
---

<span style="color: green;">SUCCESS</span>

```bash
curl -u mm54764:123456 http://localhost:8080/api/users
```
**GET USER BY ID**
---

<span style="color: green;">SUCCESS</span>

```bash
curl -u mm54764:123456 http://localhost:8080/api/users/6d6f5582-fb42-4ee9-a1f4-d870cc7b9836
```
<span style="color: red;">ERROR - not found</span>

```bash
curl -u mm54764:123456 http://localhost:8080/api/users/6d6f5582-fb42-4ee9-a1f4-d870cc7b0000
```

**UPDATE USER**
---
<span style="color: green;">SUCCESS</span>

```bash
#!/bin/bash

# Basic auth credentials
authCredentials="cGJhX3VzZXI6MTIzNDU2"

# Authentication and user URLs
authUrl="https://pba-auth-server.herokuapp.com/oauth/token"
userID="6d6f5582-fb42-4ee9-a1f4-d870cc7b9836"
userURL="http://localhost:8080/api/users/$userID"


guid=$(uuidgen)
sendDate=$(date -u +"%Y-%m-%dT%H:%M:%SZ")

authResponse=$(curl -X POST "$authUrl" \
    -H "Authorization: Basic $authCredentials" \
    -H "Content-Type: application/x-www-form-urlencoded" \
    -d "grant_type=client_credentials")

accessToken=$(echo "$authResponse" | grep -o '"access_token":"[^"]*' | awk -F'"' '{print $4}')

userData=$(cat <<EOF
{
    "requestHeader": {
        "requestId": "$guid",
        "sendDate": "$sendDate"
    },
    "user": {
        "personalId": "92011165987",
        "surname": "Kowalski",
        "citizenship": "PL",
        "name": "Jan",
        "age": 12,
        "email": "e_musk@gmail.com"
    }
}
EOF
)

userResponse=$(curl -X PUT "$userURL" \
    -H "Authorization: Bearer $accessToken" \
    -H "Content-Type: application/json" \
    -d "$userData")


if [ $? -eq 0 ]; then
    echo "Request successful."
    echo "Response: $userResponse"
else
    echo "Request failed."
fi

```

<span style="color: red;">ERROR - wrong personal id format</span>

```bash
#!/bin/bash

# Basic auth credentials
authCredentials="cGJhX3VzZXI6MTIzNDU2"

# Authentication and user URLs
authUrl="https://pba-auth-server.herokuapp.com/oauth/token"
userID="6d6f5582-fb42-4ee9-a1f4-d870cc7b9836"
userURL="http://localhost:8080/api/users/$userID"

# Wrong personal ID
personalID="9201116598731231231231231231"

guid=$(uuidgen)
sendDate=$(date -u +"%Y-%m-%dT%H:%M:%SZ")

authResponse=$(curl -X POST "$authUrl" \
    -H "Authorization: Basic $authCredentials" \
    -H "Content-Type: application/x-www-form-urlencoded" \
    -d "grant_type=client_credentials")

accessToken=$(echo "$authResponse" | grep -o '"access_token":"[^"]*' | awk -F'"' '{print $4}')

userData=$(cat <<EOF
{
    "requestHeader": {
        "requestId": "$guid",
        "sendDate": "$sendDate"
    },
    "user": {
        "personalId": "$personalID",
        "surname": "Kowalski",
        "citizenship": "PL",
        "name": "Jan",
        "age": 12,
        "email": "e_musk@gmail.com"
    }
}
EOF
)

userResponse=$(curl -X PUT "$userURL" \
    -H "Authorization: Bearer $accessToken" \
    -H "Content-Type: application/json" \
    -d "$userData")


if [ $? -eq 0 ]; then
    echo "Request successful."
    echo "Response: $userResponse"
else
    echo "Request failed."
fi


```

**DELETE USER**
---

<span style="color: green;">SUCCESS</span>

```bash
#!/bin/bash

# Basic auth credentials
authCredentials="cGJhX3VzZXI6MTIzNDU2"

# Authentication and user URLs
authUrl="https://pba-auth-server.herokuapp.com/oauth/token"
userID="6d6f5582-fb42-4ee9-a1f4-d870cc7b9836"
userURL="http://localhost:8080/api/users/$userID"

guid=$(uuidgen)
sendDate=$(date -u +"%Y-%m-%dT%H:%M:%SZ")

authResponse=$(curl -X POST "$authUrl" \
    -H "Authorization: Basic $authCredentials" \
    -H "Content-Type: application/x-www-form-urlencoded" \
    -d "grant_type=client_credentials")

accessToken=$(echo "$authResponse" | grep -o '"access_token":"[^"]*' | awk -F'"' '{print $4}')

userResponse=$(curl -X DELETE "$userURL" \
    -H "Authorization: Bearer $accessToken")

if [ $? -eq 0 ]; then
    echo "Request successful."
    echo "Response: $userResponse"
else
    echo "Request failed."
fi

```

<span style="color: red;">ERROR - not found</span>

```bash
#!/bin/bash

# Basic auth credentials
authCredentials="cGJhX3VzZXI6MTIzNDU2"

# Authentication and user URLs
authUrl="https://pba-auth-server.herokuapp.com/oauth/token"
# Wrong id
userID="6d6f5582-fb42-4ee9-a1f4-d870cc7b0000"
userURL="http://localhost:8080/api/users/$userID"

guid=$(uuidgen)
sendDate=$(date -u +"%Y-%m-%dT%H:%M:%SZ")

authResponse=$(curl -X POST "$authUrl" \
    -H "Authorization: Basic $authCredentials" \
    -H "Content-Type: application/x-www-form-urlencoded" \
    -d "grant_type=client_credentials")

accessToken=$(echo "$authResponse" | grep -o '"access_token":"[^"]*' | awk -F'"' '{print $4}')

userResponse=$(curl -X DELETE "$userURL" \
    -H "Authorization: Bearer $accessToken")

if [ $? -eq 0 ]; then
    echo "Request successful."
    echo "Response: $userResponse"
else
    echo "Request failed."
fi

```

## Lab5

### Zadanie 1

### Basic Authentication
**Opis:**

- Prosty mechanizm uwierzytelniania w protokole HTTP.
- Używa standardowego nagłówka HTTP, z kodowaniem nazwy użytkownika i hasła w base64.
- Nazwa użytkownika i hasło wysyłane są przy każdym żądaniu do serwera.

**Zalety:**

- Prostota implementacji i stosowania.
- Nie wymaga dodatkowej infrastruktury po stronie serwera.

**Wady:**

- Narażenie na ryzyko, jeśli połączenie nie jest zabezpieczone protokołem HTTPS.
- Brak dodatkowych mechanizmów zabezpieczających, takich jak tokeny z ograniczonym czasem ważności.

**Zastosowanie:**

- Odpowiedni dla prostych aplikacji wewnętrznych lub środowisk testowych.
- Przydatny, gdy bezpieczeństwo nie jest priorytetem i istnieje pełne zaufanie do sieci.

### OAuth2.0 (Client Credentials Grant)

**Opis:**

- Framework autoryzacji umożliwiający aplikacjom uzyskiwanie ograniczonego dostępu do kont użytkowników na zewnętrznych serwerach.
- 'Client Credentials Grant' to przepływ przeznaczony dla komunikacji serwer-do-serwera, gdzie aplikacja autoryzuje się własnymi poświadczeniami.

**Zalety:**

- Bezpieczniejszy niż Basic Authentication, szczególnie w komunikacji między aplikacjami.
- Tokeny dostępu mogą być ograniczone czasowo i zakresem uprawnień.

**Wady:**

- Skomplikowany w implementacji i zarządzaniu.
- Wymaga infrastruktury do zarządzania tokenami.

**Zastosowanie:**

- Idealny dla aplikacji wymagających wysokiego poziomu bezpieczeństwa i kontroli dostępu, np. w aplikacjach internetowych lub mobilnych.
- Zalecany, gdy wymagane jest delegowanie uprawnień dostępu do zasobów bez udostępniania rzeczywistych poświadczeń użytkownika.

### Zadanie 2

1. Zaimplementować autoryzacje w oparciu o Basic Authentication. Dane logowania zgodnie ze wzorcem [numer_albumu]/123456. np. bm52123/123456.
2. Wykonać próbne zapytanie ze złymi danymi logowania i zaprezentować, że zostało ono odrzucone oraz odpowiedź systemu jest zgodna ze specyfikacją.
3. Wykonać poprawne zapytanie z prawidłowymi danymi logowania i zaprezentować, że zostało ono przetworzone oraz odpowiedź systemu jest zgodna ze specyfikacją.

![zad2](docs/images/zad2-1-2.png)

### Zadanie 3

Zaimplementować autoryzacje w oparciu o mechanizm OAuth2.0 Client Credentials Grant. Implementacja powinna wykorzystywać tokeny JWT generowane przez service autoryzacyjny dostępny pod wskazanym adresem URL: https://pba-auth-server.herokuapp.com. Do weryfikacji tokenów JWT należy wykorzystać certyfikat klucza publicznego.

- Pozyskanie tokena jwt przez Postman

![get_access_token](docs/images/get_access_token_postman.png)

- Weryfikacja tokenu przez [jwt.io](https://jwt.io/)

![token_verification](docs/images/token-verification-jwt.png)

- Poprawne zapytanie

![oauth-authorized](docs/images/oauth-authorized.png)

- Brak uwierzytelnienia

![oauth-unauthorized](docs/images/oauth-unauthorized.png)

- Token wygasł

![oauth-expired](docs/images/access_token_expired.png)


